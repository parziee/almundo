import { Injectable } from '@angular/core';
import * as data from 'src/app/data/data.json';
import { Hotel } from 'src/app/entities/hotel.js';
import { HttpClient } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class HotelService {

    private baseApiUrl = ENV.url;

    constructor(
        private httpClient: HttpClient
    ) {
    }

    getHotels(): Observable<any> {
        return this.httpClient
            .get<Array<Hotel>>(`${this.baseApiUrl}/hotels`);
    }

    getHotelsByFilter(stars: string = '', name: string = ''): Observable<any> {

        return this.httpClient
            .get<Array<Hotel>>(`${this.baseApiUrl}/hotels/filterBy/${name}/${stars}`);
    }

}
