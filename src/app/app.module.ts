import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FilterComponent } from './components/filter/filter.component';
import { MatSelectModule, MatInputModule, MatExpansionModule, MatCardModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponent } from './components/card/card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HotelService } from './services/hotels/hotels.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatInputModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    NgbModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterComponent,
    CardComponent,
  ],
  providers: [
    HotelService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
