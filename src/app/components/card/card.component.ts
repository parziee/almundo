import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})

export class CardComponent {

    public name;
    public check;
    @Input() hotels = [];

    constructor() { }

    getStars(n) {
        return Array(n).fill('*');
    }

}
