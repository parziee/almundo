import { Component, Output, EventEmitter } from '@angular/core';
import { HotelService } from 'src/app/services/hotels/hotels.service';
import { Hotel } from 'src/app/entities/hotel';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss']
})

export class FilterComponent {

    @Output() filteredHotels = new EventEmitter<Array<Hotel>>();

    public STARS = [1, 2, 3, 4, 5];
    private readonly DEFAULT_STARS = '1,2,3,4,5';
    public name;
    public check = false;
    public stars: any = {};
    public hotels: any = [];

    constructor(
        private hotelService: HotelService,
    ) { }

    getStars(n) {
        return Array(n).fill('*');
    }

    filter() {
        const stars = document.querySelectorAll('.stars-checkbox') as unknown as Array<HTMLInputElement>;
        let stringStars = '';

        stars.forEach(star => {
            if (star.checked) {
                stringStars = `${stringStars}${star.value},`;
            }
        });

        if (stringStars.length > 0) {
            stringStars = stringStars.slice(0, -1);
        } else {
            stringStars = this.DEFAULT_STARS;
        }

        this.hotelService.getHotelsByFilter(stringStars, this.name).subscribe(res => {
            this.hotels = res;
            this.filteredHotels.emit(this.hotels);
        });
    }

}
