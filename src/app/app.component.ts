import { Component, OnInit } from '@angular/core';
import { HotelService } from './services/hotels/hotels.service';
import { Hotel } from 'src/app/entities/hotel';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

    public hotels: Array<Hotel> = [];

    constructor(
        private hotelService: HotelService,
    ) { }

    ngOnInit() {
        this.getHotels();
    }

    getHotels() {
        this.hotelService.getHotels().subscribe(res => {
            this.hotels = res;
        });
    }

    filteredHotels($event) {
        this.hotels = $event;
    }

}
