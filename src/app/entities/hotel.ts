export class Hotel {
    public id: string;
    public name: string;
    public stars: number;
    public price: number;
    public image: any;
    public amenities: Array<string>;

    public constructor(init?: Hotel) {
        Object.assign(this, init);
    }
}
